package main;

public class Tesla implements Car {
    protected String brand = "Tesla";
    protected String model;
    protected int maxSpeed;

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void drive() {
        System.out.println("Вы ведете " + brand + " " + model + " максимальная скорость " + maxSpeed);
    }
}
