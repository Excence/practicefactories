package main;

public class AbsctFactory {
    public Factory createFactory(String typeOfFactory){
        switch (typeOfFactory){
            case "Car" : return new CarFactory();
            case "Ship" : return new ShipFactory();
            default : return null;
        }
    }
}
