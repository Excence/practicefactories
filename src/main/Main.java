package main;

public class Main {
    public static void main(String[] args) {
        Factory factoryOFcars = new AbsctFactory().createFactory("Car");
        Factory factoryOFships = new AbsctFactory().createFactory("Ship");

        Car tesla = factoryOFcars.createCar("Tesla");

        tesla.setModel("model S");
        tesla.setMaxSpeed(250);
        tesla.drive();

        Car mazda = factoryOFcars.createCar("Mazda");

        mazda.setModel("CX-5");
        mazda.setMaxSpeed(200);
        mazda.drive();

        Ship frigate = factoryOFships.createShip("Frigate");

        frigate.setName("Normandy");
        frigate.setMaxSpeed(55);
        frigate.sail();

        Ship cruiser = factoryOFships.createShip("Cruiser");

        cruiser.setName("Ishimura");
        cruiser.setMaxSpeed(34);
        cruiser.sail();
    }
}
