package main;

public class Frigate implements Ship {
    private String type = "Frigate";
    protected String name;
    protected int maxSpeed;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getMaxSpeed() {
        return 0;
    }

    @Override
    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void sail() {
        System.out.println("Вы плывете на " + type + " «" + name + "» максимальная скорость " + maxSpeed);
    }
}
