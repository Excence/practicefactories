package main;

public interface Factory {
    Car createCar(String type);
    Ship createShip(String type);
}
