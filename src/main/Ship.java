package main;

public interface Ship {
    String getType();
    String getName();
    void setName(String model);
    int getMaxSpeed();
    void setMaxSpeed(int maxSpeed);
    void sail();
}
