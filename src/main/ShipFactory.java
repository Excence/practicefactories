package main;

public class ShipFactory implements Factory {
    public Ship createShip(String type){
        switch (type){
            case "Frigate" : return new Frigate();
            case "Cruiser" : return new Cruiser();
            default : return null;
        }
    }

    @Override
    public Car createCar(String type) {
        return null;
    }
}
