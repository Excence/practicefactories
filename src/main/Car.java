package main;

public interface Car {
    String getBrand();
    String getModel();
    void setModel(String model);
    int getMaxSpeed();
    void setMaxSpeed(int maxSpeed);
    void drive();
}
