package main;

public class CarFactory implements Factory{
    public Car createCar(String type){
        switch (type){
            case "Tesla" : return new Tesla();
            case "Mazda" : return new Mazda();
            default : return null;
        }
    }

    @Override
    public Ship createShip(String type) {
        return null;
    }
}
